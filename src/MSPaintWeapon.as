package {
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MSPaintWeapon extends Weapon {
		[Embed(source='../sfx/mspaint.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function MSPaintWeapon() {
			super(10, 100);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			sfx.play();
			var bullet:MSPaintBullet = GameWorld.world().create(MSPaintBullet) as MSPaintBullet;
			bullet.reset(x, y, angle);
		}
	}
}
