package {
	import flash.net.drm.DRMVoucherDownloadContext;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Enemy extends Entity {
		[Embed(source='../sfx/enemyhit.mp3')]
		private static const kHitSfxFile:Class;
		private var hitSfx:Sfx = new Sfx(kHitSfxFile);

		[Embed(source='../sfx/enemydie.mp3')]
		private static const kDieSfxFile:Class;
		private var dieSfx:Sfx = new Sfx(kDieSfxFile);

		public var health:Number;
		public var cooldown:int;
		
		protected var cooldownTimer:int;
		
		public function Enemy() {
			type = "enemy";
			layer = -50;
		}
		
		public function init(x:Number, y:Number, cooldown:int, health:Number):void {
			this.health = health;
			this.x = x;
			this.y = y;
			this.cooldown = cooldown;
			
			cooldownTimer = FP.rand(cooldown);
		}
		
		override public function update():void {
			var player:Player = FP.world.getInstance("player");
			
			if (Math.abs(x - player.x) > FP.width * 0.66 || Math.abs(y - player.y) > FP.height * 0.66) {
				// Don't update off-screen enemies.
				if (GameWorld.levelNumber != 4 || (this is Jukebox)) {
					// Except gauntlet mode.
					return;
				}
			}
			
			if (player.health <= 0) {
				// Don't update enemies when you're dead.
				return;
			}
			
			var angle:Number = Math.atan2(player.y - y, player.x - x);

			onMove(angle);
			
			cooldownTimer--;
			if (cooldownTimer <= 0) {
				onFire(angle * FP.DEG);
				cooldownTimer = cooldown;
			}
		}
		
		public function hit(damage:Number):void {
			if (health <= 0) {
				return;
			}
			
			health -= damage;
			hitSfx.play();
			if (health <= 0) {
				onDie();
				FP.world.recycle(this);
			}
		}
		
		protected function onFire(playerAngle:Number):void {
		}
		
		protected function onDie():void {
			dieSfx.play();
			
			GameWorld.numKills++;
		}
		
		protected function onMove(playerAngle:Number):void {
		}
		
		protected function tryMove(xVel:Number, yVel:Number):void {
			// Try moving in x.
			var prevX:Number = x;
			x += xVel;
			if (collide("level", x, y)) {
				x = prevX;
			}
			
			// Try moving in y.
			var prevY:Number = y;
			y += yVel;
			if (collide("level", x, y)) {
				y = prevY;
			}
		}
	}
}
