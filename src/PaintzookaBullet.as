package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintzookaBullet extends Bullet {
		[Embed(source='../img/particlelarge.png')]
		private static const kParticleFile:Class;

		[Embed(source='../img/paintzookabullet.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/paintzookaexplode.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		private static const kSpeed:Number = 10.0;
		
		private static const kDamage:Number = 10.0;
		
		public function PaintzookaBullet() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
			image.smooth = true;
		}
		
		public function reset(x:Number, y:Number, angle:Number):void
		{
			super.init(x, y, angle, kSpeed, 1);
			image.angle = angle;
		}

		override protected function onEnemyCollide(enemy:Enemy):void {
			enemy.hit(kDamage);
			explode();
			FP.world.recycle(this);
		}

		override protected function explode():void {
			sfx.play();
			ParticleExplosion.make(x, y, kParticleFile, 8, 8, 50, 64, 128, 4, 24, 0xFFFFFF);

			var bullet:PaintbrushBullet = GameWorld.world().create(PaintbrushBullet) as PaintbrushBullet;
			bullet.reset(x, y, angle, true);
		}
	}
}
