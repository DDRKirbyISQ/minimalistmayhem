package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Heart extends Powerup {
		[Embed(source='../img/heartpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		public function Heart() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
		}
		
		override protected function onPickup():void {
			var player:Player = world.getInstance("player") as Player;
			
			if (GameWorld.difficulty == 0) {
				// Full health restore for easy difficulty.
				player.health = Player.maxHealth
			} else if (player.health < Player.maxHealth) {
				player.health++;
			}
		}
	}
}
