package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MenuWorld extends World {
		[Embed(source='../mus/MinimalistMenu.mp3')]
		private static const kMusicFile:Class;
		private var music:Sfx = new Sfx(kMusicFile);
		
		[Embed(source='../sfx/menu.mp3')]
		private static const kMenuSfxFile:Class;
		private var menuSfx:Sfx = new Sfx(kMenuSfxFile);

		[Embed(source='../sfx/gamestart.mp3')]
		private static const kGameStartSfxFile:Class;
		private var gameStartSfx:Sfx = new Sfx(kGameStartSfxFile);

		[Embed(source='../img/titleplayer.png')]
		private static const kPlayerFile:Class;
		private var playerImage:Image = new Image(kPlayerFile);
		private var playerTween:LinearMotion = new LinearMotion;
		
		[Embed(source='../img/titleminimalist.png')]
		private static const kMinimalistFile:Class;
		private var minimalistImage:Image = new Image(kMinimalistFile);
		private var minimalistTween:LinearMotion = new LinearMotion;
		
		[Embed(source='../img/titlemayhem.png')]
		private static const kMayhemFile:Class;
		private var mayhemImage:Image = new Image(kMayhemFile);
		private var mayhemTween:LinearMotion = new LinearMotion;

		[Embed(source='../img/titleddrkirby.png')]
		private static const kDdrkirbyFile:Class;
		private var ddrkirbyImage:Image = new Image(kDdrkirbyFile);
		
		private var startText:Text = new Text("Start Game", FP.halfWidth, 420);
		private var difficultyText:Text = new Text("Difficulty: Normal", FP.halfWidth, 460);
		private var gauntletText:Text = new Text("Gauntlet Mode", FP.halfWidth, 500);
		
		[Embed(source='../img/particlesmall.png')]
		private static const kFadeFile:Class;
		private var fadeImage:Image = new Image(kFadeFile);
		
		private var cursorImage:Image = new Image(kPlayerFile);
		
		private var selectedChoice:int = 0;
		
		private var timer:int = 0;
		
		private var fading:Boolean = false;
		private var fadeTimer:int = 0;
		
		private static const kFadeDuration:int = 60;
		private static const kMusicFadeDuration:int = 120;
		
		public function MenuWorld() {
			FP.screen.color = 0x404040;
			music.loop();
			
			addGraphic(playerImage);
			playerImage.centerOrigin();
			playerImage.x = -150;
			playerImage.y = -100;
			playerImage.scale = 5;
			playerTween.setMotion(-100, -60, FP.halfWidth, FP.halfHeight, 190);
			addTween(playerTween);
			
			addGraphic(minimalistImage);
			minimalistImage.scale = 6;
			minimalistImage.centerOrigin();
			minimalistImage.x = 1000;
			minimalistTween.setMotion(1000, 70, FP.halfWidth, 70, 180, Ease.cubeOut);
			
			addGraphic(mayhemImage);
			mayhemImage.scale = 6;
			mayhemImage.centerOrigin();
			mayhemImage.x = FP.halfWidth;
			mayhemImage.y = 130;
			mayhemImage.visible = false;
			
			addGraphic(ddrkirbyImage);
			ddrkirbyImage.scale = 2;
			ddrkirbyImage.centerOrigin();
			ddrkirbyImage.x = FP.halfWidth;
			ddrkirbyImage.y = 180;
			ddrkirbyImage.visible = false;
			
			startText.centerOrigin();
			startText.scale = 2;
			startText.visible = false;
			addGraphic(startText);

			difficultyText.centerOrigin();
			difficultyText.scale = 2;
			difficultyText.visible = false;
			addGraphic(difficultyText);
			
			gauntletText.centerOrigin();
			gauntletText.scale = 2;
			gauntletText.visible = false;
			addGraphic(gauntletText);
			
			cursorImage.centerOrigin();
			cursorImage.visible = false;
			addGraphic(cursorImage);
			
			addGraphic(fadeImage);
			fadeImage.scaleX = 800;
			fadeImage.scaleY = 600;
			fadeImage.alpha = 0;
			fadeImage.color = 0x000000;
		}
		
		override public function begin():void {
			var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0x000000, 1.0, 0.01);
		}
		
		override public function update():void {
			// Update all entities.
			super.update();

			timer++;
			
			if (timer == 180) {
				addTween(minimalistTween, true);
			}
			
			if (timer == 390) {
				var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 1.0, 0.05);
				mayhemImage.visible = true;
				ddrkirbyImage.visible = true;
				startText.visible = true;
				difficultyText.visible = true;
				cursorImage.visible = true;
				gauntletText.visible = true;
			}
			
			playerImage.x = playerTween.x;
			playerImage.y = playerTween.y;
			minimalistImage.x = minimalistTween.x;
			minimalistImage.y = minimalistTween.y;
			
			if (fading) {
				fadeImage.alpha = fadeTimer / kFadeDuration;
				music.volume = 1.0 - (fadeTimer / kMusicFadeDuration);
			}
			
			var difficultyString:String = GameWorld.difficultyString();

			difficultyText.text = "Difficulty: " + difficultyString;
			difficultyText.centerOrigin();
			
			cursorImage.x = 220;
			cursorImage.y = (cursorImage.y + menuY()) / 2;
			if (Math.abs(cursorImage.y - menuY()) < 1.0) {
				cursorImage.y = menuY();
			}
			
			handleInput();
		}
		
		private function menuY():Number {
			switch (selectedChoice) {
				case 0:
					return 420;
				case 1:
					return 460;
				case 2:
					return 500;
			}
			return 0;
		}
		
		private function handleInput():void {
			if (fading) {
				fadeTimer++;
				if (fadeTimer > kMusicFadeDuration) {
					startGame();
				}
				return;
			}
			
			if (timer < 390) {
				return;
			}
			
			if (Input.pressed(Key.DOWN)) {
				selectedChoice++;
				menuSfx.play();
			}
			if (Input.pressed(Key.UP)) {
				selectedChoice--;
				menuSfx.play();
			}
			selectedChoice = (selectedChoice + 3) % 3;
			
			if (Input.pressed(Key.ENTER) || Input.pressed("fire")) {
				switch (selectedChoice) {
					case 0:
						gameStartSfx.play();
						fading = true;
						break;
					case 1:
						menuSfx.play();
						GameWorld.difficulty++;
						break;
					case 2:
						gameStartSfx.play();
						fading = true;
						break;
				}
			}
			
			if (Input.pressed(Key.LEFT) && selectedChoice == 1) {
				GameWorld.difficulty--;
				menuSfx.play();
			}
			if (Input.pressed(Key.RIGHT) && selectedChoice == 1) {
				GameWorld.difficulty++;
				menuSfx.play();
			}
			
			GameWorld.difficulty = (GameWorld.difficulty + 3) % 3;
		}
		
		private function startGame():void {
			music.stop();
			GameWorld.continuesUsed = 0;
			if (selectedChoice == 2) {
				// gauntlet
				GameWorld.levelNumber = 4;
				GameWorld.numKills = 0;
				GameWorld.continuesUsed = 0;
				FP.world = new GameWorld(4);
			} else {
				GameWorld.levelNumber = 1;
				FP.world = new StoryWorld();
			}
		}
	}
}
