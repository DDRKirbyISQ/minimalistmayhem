package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintballBullet extends Bullet {
		[Embed(source='../img/particlelarge.png')]
		private static const kParticleFile:Class;

		[Embed(source='../img/paintballbullet.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		private static const kSpeed:Number = 10.0;
		
		private static const kDamage:Number = 2.0;
		
		public function PaintballBullet() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, angle:Number):void
		{
			super.init(x, y, angle, kSpeed, 1);
		}

		override protected function onEnemyCollide(enemy:Enemy):void {
			enemy.hit(kDamage);
			explode();
			FP.world.recycle(this);
		}

		override protected function explode():void {
			ParticleExplosion.make(x, y, kParticleFile, 8, 8, 10, 48, 48, 8, 8, 0xFFFFFF);
		}
	}
}
