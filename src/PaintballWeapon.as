package {
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintballWeapon extends Weapon {
		[Embed(source='../sfx/paintball.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function PaintballWeapon() {
			super(6, 100);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			sfx.play();
			var bullet:PaintballBullet = GameWorld.world().create(PaintballBullet) as PaintballBullet;
			bullet.reset(x, y, angle);
		}
	}
}
