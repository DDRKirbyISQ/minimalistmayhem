package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class StoryWorld extends World {
		[Embed(source='../img/story1.png')]
		private static const kImage1:Class;
		private var Image1:Image = new Image(kImage1);
		[Embed(source='../img/story2.png')]
		private static const kImage2:Class;
		private var Image2:Image = new Image(kImage2);
		[Embed(source='../img/story3.png')]
		private static const kImage3:Class;
		private var Image3:Image = new Image(kImage3);
		[Embed(source='../img/story4.png')]
		private static const kImage4:Class;
		private var Image4:Image = new Image(kImage4);
		[Embed(source='../img/story5.png')]
		private static const kImage5:Class;
		private var Image5:Image = new Image(kImage5);
		[Embed(source='../img/story6.png')]
		private static const kImage6:Class;
		private var Image6:Image = new Image(kImage6);
		[Embed(source='../img/story7.png')]
		private static const kImage7:Class;
		private var Image7:Image = new Image(kImage7);
		[Embed(source='../img/story8.png')]
		private static const kImage8:Class;
		private var Image8:Image = new Image(kImage8);
		[Embed(source='../img/story9.png')]
		private static const kImage9:Class;
		private var Image9:Image = new Image(kImage9);
		[Embed(source='../img/story10.png')]
		private static const kImage10:Class;
		private var Image10:Image = new Image(kImage10);
		[Embed(source='../img/story11.png')]
		private static const kImage11:Class;
		private var Image11:Image = new Image(kImage11);
		[Embed(source='../img/story12.png')]
		private static const kImage12:Class;
		private var Image12:Image = new Image(kImage12);
		[Embed(source='../img/story13.png')]
		private static const kImage13:Class;
		private var Image13:Image = new Image(kImage13);
		[Embed(source='../img/story14.png')]
		private static const kImage14:Class;
		private var Image14:Image = new Image(kImage14);
		[Embed(source='../img/story15.png')]
		private static const kImage15:Class;
		private var Image15:Image = new Image(kImage15);
		[Embed(source='../img/story16.png')]
		private static const kImage16:Class;
		private var Image16:Image = new Image(kImage16);
		
		private var images:Vector.<Image> = Vector.<Image>([Image1, Image2, Image3, Image4, Image5, Image6, Image7, Image8, Image9, Image10, Image11, Image12, Image13, Image14, Image15, Image16]);
		
		[Embed(source='../mus/MinimalistMenu.mp3')]
		private static const kMusicFile:Class;
		private var music:Sfx = new Sfx(kMusicFile);
		
		[Embed(source='../sfx/menu.mp3')]
		private static const kMenuSfxFile:Class;
		private var menuSfx:Sfx = new Sfx(kMenuSfxFile);
		
		[Embed(source='../sfx/gamestart.mp3')]
		private static const kGameStartSfxFile:Class;
		private var gameStartSfx:Sfx = new Sfx(kGameStartSfxFile);
		
		[Embed(source='../img/particlesmall.png')]
		private static const kFadeFile:Class;
		private var fadeImage:Image = new Image(kFadeFile);
		
		private var timer:int = 0;
		
		private var fading:Boolean = false;
		private var fadeTimer:int = 0;
		
		private static const kFadeDuration:int = 60;
		private static const kMusicFadeDuration:int = 120;
		
		private var step:int = 0;
		
		public function StoryWorld() {
			FP.screen.color = 0x404040;
			
			//music.loop();
			
			for each (var image:Image in images) {
				addGraphic(image, 0, 0);
				image.visible = false;
			}
			
			var text:Text = new Text("Press ENTER to Continue", FP.halfWidth, FP.height - 10);
			text.centerOrigin();
			text.color = 0xFFFF00;
			addGraphic(text);
			
			images[0].visible = true;

			addGraphic(fadeImage);
			fadeImage.scaleX = 800;
			fadeImage.scaleY = 600;
			fadeImage.alpha = 0;
			fadeImage.color = 0x000000;
		
		}
		
		override public function begin():void {
			var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0x000000, 1.0, 0.05);
		}
		
		override public function update():void {
			for (var i:int = 0; i < 16; ++i) {
				if (i == step) {
					images[i].visible = true;
				} else {
					images[i].visible = false;
				}
			}
			
			// Update all entities.
			super.update();
			
			timer++;
			
			if (fading) {
				fadeImage.alpha = fadeTimer / kFadeDuration;
//				music.volume = 1.0 - (fadeTimer / kMusicFadeDuration);
			}
			
			handleInput();
		}
		
		private function handleInput():void {
			if (fading) {
				fadeTimer++;
				if (fadeTimer > kFadeDuration + 30) {
					startGame();
				}
				return;
			}
			
			if (Input.pressed(Key.ENTER) || Input.pressed("fire")) {
				
				if (step >= 15) {
					gameStartSfx.play();
					fading = true;
				} else {
					menuSfx.play();
					step++;
				}
			}
		}
		
		private function startGame():void {
//			music.stop();
			GameWorld.continuesUsed = 0;
			GameWorld.numKills = 0;
			FP.world = new GameWorld(1);
		}
	}
}
