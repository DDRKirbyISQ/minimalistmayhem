package  
{
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ScoreWorld extends World 
	{
		[Embed(source='../sfx/gamestart.mp3')]
		private static const kGameStartSfxFile:Class;
		private var gameStartSfx:Sfx = new Sfx(kGameStartSfxFile);

		[Embed(source='../img/particlesmall.png')]
		private static const kFadeFile:Class;
		private var fadeImage:Image = new Image(kFadeFile);
		
		[Embed(source='../img/titleplayer.png')]
		private static const kPlayerFile:Class;
		private var playerImage:Image = new Image(kPlayerFile);
		
		private var timer:int = 0;
		
		private var fading:Boolean = false;
		private var fadeTimer:int = 0;
		
		private static const kFadeDuration:int = 120;
		private static const kMusicFadeDuration:int = 120;
		
		public function ScoreWorld() 
		{
			FP.screen.color = 0x404040;

			var text:Text = new Text("Press ENTER to Continue", FP.halfWidth, FP.height - 10);
			text.centerOrigin();
			text.color = 0xFFFF00;
			addGraphic(text);

			playerImage.centerOrigin();
			playerImage.x = FP.halfWidth;
			playerImage.y = FP.height - 180;
			addGraphic(playerImage);
			
			var text2:Text = new Text("Thank you for playing", FP.halfWidth, FP.height - 150);
			text2.centerOrigin();
			addGraphic(text2);

			var text3:Text = new Text("MINIMALIST MAYHEM", FP.halfWidth, 100);
			text3.centerOrigin();
			addGraphic(text3);

			var text5:Text = new Text("Made in 48 hours for Ludum Dare", FP.halfWidth, 120);
			text5.centerOrigin();
			addGraphic(text5);

			var text6:Text = new Text("by DDRKirby(ISQ)", FP.halfWidth, 140);
			text6.centerOrigin();
			addGraphic(text6);

			var text4:Text = new Text("Results", FP.halfWidth, 180);
			text4.centerOrigin();
			addGraphic(text4);

			var text7:Text = new Text("Difficulty: " + GameWorld.difficultyString(), FP.halfWidth, 200);
			text7.centerOrigin();
			addGraphic(text7);

			var time1:String = timeString(GameWorld.times[1] + 0);
			var time2:String = timeString(GameWorld.times[2] + 0);
			var time3:String = timeString(GameWorld.times[3] + 0);
			var totaltime:String = timeString(GameWorld.times[1] + GameWorld.times[2] + GameWorld.times[3]);
			addGraphic(new Text("Level 1 Time: "+ time1, 330, 220));
			addGraphic(new Text("Level 2 Time: "+ time2, 330, 240));
			addGraphic(new Text("Level 3 Time: " + time3, 330, 260));
			var arstText:Text = new Text("Total Time: " + totaltime, 330, 280);
			arstText.color = 0xFFFF00;
			addGraphic(arstText);

			addGraphic(new Text("Kills: "+ GameWorld.numKills, 330, 320));
			addGraphic(new Text("Continues: "+ GameWorld.continuesUsed, 330, 340));

			addGraphic(fadeImage);
			fadeImage.scaleX = 800;
			fadeImage.scaleY = 600;
			fadeImage.alpha = 0;
			fadeImage.color = 0x000000;
		}
		
		override public function begin():void {
			var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0x000000, 1.0, 0.01);
		}
		
		override public function update():void
		{
			super.update();
			if (fading) {
				fadeImage.alpha = fadeTimer / kFadeDuration;
				//music.volume = 1.0 - (fadeTimer / kMusicFadeDuration);

				fadeTimer++;
				if (fadeTimer > kFadeDuration) {
					FP.world = new MenuWorld;
				}
				return;
			}
			
			if (Input.pressed(Key.ENTER) || Input.pressed("fire")) {
				gameStartSfx.play();
				fading = true;
			}
		}
		
		private function timeString(time:int):String {
			var seconds:Number = time / 60.0;
			var sec:String = Math.floor(seconds % 60).toString();
			if (Math.floor(seconds % 60) < 10) {
				sec = "0" + sec;
			}
			var min:String = Math.floor(seconds / 60).toString();
			if (Math.floor(seconds / 60) < 10) {
				min = "0" + min;
			}
			return min.toString() + ":" + sec.toString();
		}
	}
}
