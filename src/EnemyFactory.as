package {
	import flash.sampler.NewObjectSample;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class EnemyFactory extends Enemy {
		private static var kEnemyLimit:int = 15;
		
		[Embed(source='../img/enemyfactory.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/enemyfactorydie.mp3')]
		private static const kDieSfxFile:Class;
		private var dieSfx:Sfx = new Sfx(kDieSfxFile);
		
		private static const kHealth:Number = 50;
		
		public function EnemyFactory() {
			super();
			
			graphic = image;
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = height;
			originX = halfWidth;
			originY = height;
			layer = -25;
		}
		
		public function reset(x:Number, y:Number):void {
			if (GameWorld.levelNumber == 4) {
				// GAUNTLET
				kEnemyLimit = 15;
			} else {
				kEnemyLimit = 15;
			}
			
			var cooldown:int;
			switch (GameWorld.difficulty) {
				case 0: 
					cooldown = 180;
					break;
				case 1: 
					cooldown = 120;
					break;
				case 2: 
					cooldown = 80;
					break;
			}
			
			init(x, y, cooldown, kHealth);
			
		}
		
		override protected function onFire(playerAngle:Number):void {
			if (GameWorld.world().enemiesOnScreen() >= kEnemyLimit) {
				return;
			}
			
			
			switch (FP.rand(2)) {
				case 0: 
					var christmasTreeEnemy:ChristmasTreeEnemy = world.create(ChristmasTreeEnemy) as ChristmasTreeEnemy;
					christmasTreeEnemy.reset(x, y);
					break;
				case 1: 
					var colorWheelEnemy:ColorWheelEnemy = world.create(ColorWheelEnemy) as ColorWheelEnemy;
					colorWheelEnemy.reset(x, y);
					break;
			}
		}
		
		override protected function onDie():void {
			dieSfx.play();
			
			var screenFlash:ScreenFlash = world.create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 0.5);
			
			switch (FP.rand(5)) {
				case 0: 
					var spraypaint:SpraypaintPowerup = world.create(SpraypaintPowerup) as SpraypaintPowerup;
					spraypaint.reset(x, y);
					break;
				case 1: 
					var paintball:PaintballPowerup = world.create(PaintballPowerup) as PaintballPowerup;
					paintball.reset(x, y);
					break;
				case 2: 
					var paintzooka:PaintzookaPowerup = world.create(PaintzookaPowerup) as PaintzookaPowerup;
					paintzooka.reset(x, y);
					break;
				case 3: 
					var mspaint:MSPaintPowerup = world.create(MSPaintPowerup) as MSPaintPowerup;
					mspaint.reset(x, y);
					break;
				case 4: 
					var paintspread:PaintSpreadPowerup = world.create(PaintSpreadPowerup) as PaintSpreadPowerup;
					paintspread.reset(x, y);
					break;
			}
		}
	}
}
