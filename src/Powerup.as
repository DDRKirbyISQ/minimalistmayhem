package {
	import net.flashpunk.Entity;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Powerup extends Entity {
		[Embed(source='../sfx/powerup.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);
		
		public function Powerup():void {
			layer = -75;
		}
		
		override public function update():void {
			if (collide("player", x, y)) {
				onPickup();
				sfx.play();
				world.recycle(this);
			}
		}
		
		protected function onPickup():void {
		}
	}
}
