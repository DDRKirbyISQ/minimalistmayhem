package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Hud extends Entity {
		[Embed(source='../img/hud.png')]
		private static const kHudImageFile:Class;

		[Embed(source='../img/heart.png')]
		private static const kHeartImageFile:Class;

		[Embed(source='../img/spraypaintpowerup.png')]
		private static const kSpraypaintImageFile:Class;
		private var spraypaintImage:Image = new Image(kSpraypaintImageFile);
		[Embed(source='../img/paintballpowerup.png')]
		private static const kPaintballImageFile:Class;
		private var paintballImage:Image = new Image(kPaintballImageFile);
		[Embed(source='../img/paintbrushpowerup.png')]
		private static const kPaintbrushImageFile:Class;
		private var paintbrushImage:Image = new Image(kPaintbrushImageFile);
		[Embed(source='../img/paintzookapowerup.png')]
		private static const kPaintzookaImageFile:Class;
		private var paintzookaImage:Image = new Image(kPaintzookaImageFile);
		[Embed(source='../img/mspaintpowerup.png')]
		private static const kMSPaintImageFile:Class;
		private var mspaintImage:Image = new Image(kMSPaintImageFile);
		[Embed(source='../img/paintspreadpowerup.png')]
		private static const kPaintSpreadImageFile:Class;
		private var paintspreadImage:Image = new Image(kPaintSpreadImageFile);

		private var hudImage:Image = new Image(kHudImageFile);
		
		private var versionText:Text;
		
		private var healthText:Text;
		private var weaponText:Text;
		private var ammoText:Text;
		private var roomsClearedText:Text;
		private var timeText:Text;
		private var levelText:Text;
		private var continuesText:Text;
		
		private var killsText:Text;
		
		private var heartImages:Vector.<Image> = new Vector.<Image>;
		
		public function Hud() {
			layer = -1000;
			graphic = hudImage;
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			versionText = new Text("v1.01", FP.width - 45, 0);
			addGraphic(versionText);
			versionText.scrollX = 0;
			versionText.scrollY = 0;
			
			healthText = new Text("Health:", 20, 12);
			addGraphic(healthText);
			healthText.scrollX = 0;
			healthText.scrollY = 0;
			weaponText = new Text("Weapon:", 20, 40);
			addGraphic(weaponText);
			weaponText.scrollX = 0;
			weaponText.scrollY = 0;
			ammoText = new Text("Ammo:", 20, 68);
			addGraphic(ammoText);
			ammoText.scrollX = 0;
			ammoText.scrollY = 0;
			if (GameWorld.levelNumber == 4) {
				levelText = new Text("Level: " + "GAUNTLET" + " (" + GameWorld.difficultyString() + ")", 575, 8);
			} else {
				levelText = new Text("Level: " + GameWorld.levelNumber + " (" + GameWorld.difficultyString() + ")", 575, 8);
			}
			addGraphic(levelText);
			levelText.scrollX = 0;
			levelText.scrollY = 0;
			roomsClearedText = new Text("Rooms Cleared:", 575, 28);
			addGraphic(roomsClearedText);
			roomsClearedText.scrollX = 0;
			roomsClearedText.scrollY = 0;
			timeText = new Text("Time:", 575, 48);
			addGraphic(timeText);
			timeText.scrollX = 0;
			timeText.scrollY = 0;
			continuesText = new Text("Continues Used: ", 575, 68);
			addGraphic(continuesText);
			continuesText.scrollX = 0;
			continuesText.scrollY = 0;
			killsText = new Text("Kills: ", FP.halfWidth, FP.height - 20);
			addGraphic(killsText);
			killsText.scrollX = 0;
			killsText.scrollY = 0;
			killsText.visible = false;
			killsText.centerOrigin();
			
			for (var i:int = 0; i < Player.maxHealth; ++i) {
				var heartImage:Image = new Image(kHeartImageFile);
				heartImage.scrollX = 0;
				heartImage.scrollY = 0;
				heartImage.x = 80 + i * 17;
				heartImage.y = 14;
				heartImages.push(heartImage);
				addGraphic(heartImage);
			}
			
			var weaponX:Number = 95;
			var weaponY:Number = 34;
			spraypaintImage.visible = false;
			spraypaintImage.scrollX = 0;
			spraypaintImage.scrollY = 0;
			spraypaintImage.x = weaponX;
			spraypaintImage.y = weaponY;
			addGraphic(spraypaintImage);
			paintballImage.visible = false;
			paintballImage.scrollX = 0;
			paintballImage.scrollY = 0;
			paintballImage.x = weaponX;
			paintballImage.y = weaponY;
			addGraphic(paintballImage);
			paintbrushImage.visible = false;
			paintbrushImage.scrollX = 0;
			paintbrushImage.scrollY = 0;
			paintbrushImage.x = weaponX;
			paintbrushImage.y = weaponY;
			addGraphic(paintbrushImage);
			paintzookaImage.visible = false;
			paintzookaImage.scrollX = 0;
			paintzookaImage.scrollY = 0;
			paintzookaImage.x = weaponX;
			paintzookaImage.y = weaponY;
			addGraphic(paintzookaImage);
			mspaintImage.visible = false;
			mspaintImage.scrollX = 0;
			mspaintImage.scrollY = 0;
			mspaintImage.x = weaponX;
			mspaintImage.y = weaponY;
			addGraphic(mspaintImage);
			paintspreadImage.visible = false;
			paintspreadImage.scrollX = 0;
			paintspreadImage.scrollY = 0;
			paintspreadImage.x = weaponX;
			paintspreadImage.y = weaponY;
			addGraphic(paintspreadImage);
		}
		
		override public function update():void {
			// Kills.
			killsText.visible = GameWorld.levelNumber == 4;
			killsText.text = "Kills: " + GameWorld.numKills.toString();
			
			// Health.
			for (var i:int = 0; i < Player.maxHealth; ++i) {
				var heartImage:Image = heartImages[i];
				var player:Player = world.getInstance("player");
				heartImage.visible = player.health > i;
			}
			
			// Weapon.
			spraypaintImage.visible = GameWorld.world().weapon is SpraypaintWeapon;
			paintballImage.visible = GameWorld.world().weapon is PaintballWeapon;
			paintbrushImage.visible = GameWorld.world().weapon is PaintbrushWeapon;
			paintzookaImage.visible = GameWorld.world().weapon is PaintzookaWeapon;
			mspaintImage.visible = GameWorld.world().weapon is MSPaintWeapon;
			paintspreadImage.visible = GameWorld.world().weapon is PaintSpreadWeapon;
			
			// Ammo.
			var ammo:String = GameWorld.world().weapon.ammo.toString();
			if (GameWorld.world().weapon.ammo < 0) {
				ammo = "N/A";
			}
			ammoText.text = "Ammo: " + ammo;
			
			// Time.
			var seconds:Number = GameWorld.world().gameTimer / 60.0;
			var sec:String = Math.floor(seconds % 60).toString();
			if (Math.floor(seconds % 60) < 10) {
				sec = "0" + sec;
			}
			var min:String = Math.floor(seconds / 60).toString();
			if (Math.floor(seconds / 60) < 10) {
				min = "0" + min;
			}
			timeText.text = "Time: " + min + ":" + sec;
			
			// Rooms.
			roomsClearedText.text = "Rooms Cleared: " + GameWorld.world().completedRooms() + "/" + GameWorld.world().totalRooms();
			
			// Continues.
			continuesText.text = "Continues Used: " + GameWorld.continuesUsed;
		}
	}
}
