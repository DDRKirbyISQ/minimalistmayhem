package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GameWorld extends World {
		// Difficulty is set from menu.
		// 0 = easy, 1 = normal, 2 = hard
		public static var difficulty:int = 1;
		
		public static var continuesUsed:int;
		
		[Embed(source='../img/instructions.png')]
		private static const kInstructionsFile:Class;
		private var instructionsImage:Image = new Image(kInstructionsFile);
		
		[Embed(source='../sfx/menu.mp3')]
		private static const kMenuSfxFile:Class;
		private var menuSfx:Sfx = new Sfx(kMenuSfxFile);
		
		[Embed(source='../mus/MinimalistMayhem_base.mp3')]
		private static const kMusicFile:Class;
		private var music:Sfx = new Sfx(kMusicFile);
		
		[Embed(source='../sfx/powerup.mp3')]
		private static const kContinueSfxFile:Class;
		private var continueSfx:Sfx = new Sfx(kContinueSfxFile);
		
		private static var instance:GameWorld;
		
		public var level:Level;
		public static var levelNumber:int = 1;
		
		public var weapon:Weapon;
		
		public var gameTimer:Number;
		
		public var levelCompleted:Boolean = false;
		
		private var fadeTimer:int = 0;
		
		public static var numKills:int = 0;
		
		private static var kFadeDelay:int = 120;
		private static var kFadeDuration:int = 60;
		private static var kMusicFadeDuration:int = 120;
		
		public static var times:Array = new Array;
		
		private var gauntletIntensity:int = 600;
		
		[Embed(source='../img/particlesmall.png')]
		private static const kFadeFile:Class;
		private var fadeImage:Image = new Image(kFadeFile);
		
		public var paused:Boolean = false;
		
		private var continueText:Text;
		
		//public var paintedTilesThisFrame:Vector.<Vector.<int>> = new Vector.<Vector.<int>>;
		
		// Returns you the GameWorld.  Valid even in its constructor.
		public static function world():GameWorld {
			return instance as GameWorld;
		}
		
		public function GameWorld(levelToLoad:int) {
			FP.screen.color = 0x000000;
			instance = this;
			
			levelNumber = levelToLoad;
			
			level = new Level(levelNumber);
			add(level);
			
			if (levelNumber == 4) {
				continueText = new Text("GAME OVER", FP.halfWidth, FP.halfHeight);
			} else {
				continueText = new Text("     GAME OVER\nPRESS R TO CONTINUE", FP.halfWidth, FP.halfHeight);
			}
			
			if (levelNumber == 3) {
				kFadeDelay = 240;
				kFadeDuration = 180;
				kMusicFadeDuration = 120;
			} else if (levelNumber == 4) {
				kFadeDelay = 0;
			} else {
				kFadeDelay = 120;
				kFadeDuration = 60;
				kMusicFadeDuration = 120;
			}
			
			var spawnPoint:SpawnPoint = getInstance("spawnpoint") as SpawnPoint;
			add(new Player(spawnPoint.x, spawnPoint.y));
			
			weapon = new PaintbrushWeapon;
			
			add(new Hud);
			add(new Map);
			
			fadeImage.color = 0x000000;
			fadeImage.scrollX = 0;
			fadeImage.scrollY = 0;
			fadeImage.scaleX = 800;
			fadeImage.scaleY = 600;
			fadeImage.visible = false;
			fadeImage.alpha = 0;
			addGraphic(fadeImage, -2000);
			
			continueText.scale = 3;
			continueText.scrollX = 0;
			continueText.scrollY = 0;
			continueText.visible = false;
			continueText.centerOrigin();
			addGraphic(continueText, -1000);
			
			instructionsImage.scrollX = 0;
			instructionsImage.scrollY = 0;
			instructionsImage.visible = false;
			instructionsImage.centerOrigin();
			instructionsImage.x = FP.halfWidth;
			instructionsImage.y = FP.halfHeight;
			addGraphic(instructionsImage, -3000);
			
			gameTimer = 0;
		}
		
		override public function begin():void {
			var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0x000000, 1.0, 0.05);
			var screenText:ScreenText = create(ScreenText) as ScreenText;
			if (levelNumber == 4) {
				screenText.reset("GAUNTLET MODE", 120, 0xFFFFFF, 1.0);
			} else {
				screenText.reset("LEVEL " + levelNumber + " START", 120, 0xFFFFFF, 1.0);
			}
			
			if (levelNumber == 1) {
				pause();
			}
		}
		
		override public function update():void {
			if (gameTimer == 1) {
				music.loop();
			}
			
			if (Input.pressed(Key.F3)) {
			//	GameWorld.world().completeLevel();
			}
			
			if (levelNumber == 4) {
				continueText.text = " GAME OVER\n " + numKills + " KILLS\n" + "PRESS ENTER";
				continueText.centerOrigin();
			}
			
			// Instructions.
			if ((Input.pressed(Key.F1) || Input.pressed(Key.ESCAPE) || (Input.pressed(Key.ENTER) && paused)) && !levelCompleted) {
				pause();
			}
			
			if (paused) {
				return;
			}
			
			// Update all entities.
			super.update();
			
			// Update current weapon.
			weapon.update();
			
			// Reset painted tiles.
			//paintedTilesThisFrame.length = 0;
			
			if (!levelCompleted) {
				gameTimer++;
			} else {
				fadeTimer++;
				var temp:Number = fadeTimer - kFadeDelay;
				temp = Math.max(0, temp);
				fadeImage.alpha = temp / kFadeDuration;
				music.volume = 1.0 - (fadeTimer / kMusicFadeDuration);
				
				if (fadeTimer >= kFadeDelay + kFadeDuration + 10) {
					// Load next level.
					removeAll();
					clearRecycledAll();
					music.stop();
					Jukebox.sfxArray[0].stop();
					Jukebox.sfxArray[1].stop();
					Jukebox.sfxArray[2].stop();
					Jukebox.sfxArray[3].stop();
					Jukebox.sfxArray[4].stop();
					
					if (levelNumber == 3) {
						// Win.
						FP.world = new ScoreWorld();
					} else if (levelNumber == 4) {
						// Win.
						FP.world = new MenuWorld();
					} else {
						FP.world = new GameWorld(levelNumber + 1);
					}
				}
			}
			
			// Gauntlet.
			if (levelNumber == 4) {
				handleGauntlet();
			}
			
			// Continuing.
			if (continueText.visible) {
				if (Input.pressed(Key.ENTER) || Input.pressed(Key.R)) {
					if (levelNumber == 4) {
						// gauntlet causes you to exit.
						completeLevel();
					} else {
						continueText.visible = false;
						var player:Player = getInstance("player");
						player.health = Player.maxHealth;
						player.visible = true;
						continueSfx.play();
						continuesUsed++;
						
						var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
						screenFlash.reset(0xffffff, 1.0, 0.05);
					}
				}
			}
		}
		
		private function handleGauntlet():void {
			var player:Player = getInstance("player") as Player;
			
			var factories:Array = new Array;
			getClass(EnemyFactory, factories);
			
			gauntletIntensity = 600;
			
			if (gameTimer > 900) {
				gauntletIntensity = 450;
			}
			if (gameTimer > 1800) {
				gauntletIntensity = 350;
			}
			if (gameTimer > 3600) {
				gauntletIntensity = 250;
			}
			if (gameTimer > 7200) {
				gauntletIntensity = 200;
			}
			if (gameTimer > 10800) {
				gauntletIntensity = 150;
			}
			
			if (gameTimer % gauntletIntensity == 0 && factories.length < 8) {
				var enemyFactory:EnemyFactory = create(EnemyFactory) as EnemyFactory;
				var theX:Number = player.x;
				var theY:Number = player.y;
				var theta:Number = FP.rand(360);
				theX += Math.cos(theta * FP.RAD) * FP.height * 0.5;
				theY += Math.sin(theta * FP.RAD) * FP.height * 0.5;
				if (theX < 85 * 32)
					theX = 85 * 32;
				if (theX > 112 * 32)
					theX = 112 * 32;
				if (theY < 60 * 32)
					theY = 60 * 32;
				if (theY > 93 * 32)
					theY = 93 * 32;
				enemyFactory.reset(theX, theY);
			}
		}
		
		public function completedRooms():int {
			var rooms:Vector.<Room> = new Vector.<Room>;
			getClass(Room, rooms);
			var roomsCompleted:int = 0;
			for each (var room:Room in rooms) {
				if (room.complete) {
					roomsCompleted++;
				}
			}
			return roomsCompleted;
		}
		
		public function totalRooms():int {
			var rooms:Vector.<Room> = new Vector.<Room>;
			getClass(Room, rooms);
			return rooms.length;
		}
		
		public function completeLevel():void {
			levelCompleted = true;
			fadeImage.visible = true;
			
			times[levelNumber] = gameTimer;
		}
		
		public function gameOver():void {
			var screenFlash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xffffff, 1.0, 0.05);
			continueText.visible = true;
		}
		
		static public function difficultyString():String {
			switch (difficulty) {
				case 0: 
					return "Easy";
					break;
				case 1: 
					return "Normal";
					break;
				case 2: 
					return "Hard";
					break;
			}
			
			return "";
		}
		
		public function pause():void {
			paused = !paused;
			instructionsImage.visible = paused;
			menuSfx.play();
		}
		
		public function enemiesOnScreen():int {
			var player:Player = getInstance("player") as Player;
			var result:int = 0;
			var enemies:Vector.<Enemy> = new Vector.<Enemy>;
			getClass(Enemy, enemies);
			for each (var enemy:Enemy in enemies) {
				if ((Math.abs(enemy.x - player.x) < FP.halfWidth * 1.25) && (Math.abs(enemy.y - player.y) < FP.halfHeight * 1.25)) {
					result++;
				}
			}
			
			return result;
		}
	}
}
