package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Player extends Entity {
		public static var maxHealth:int = 7;
		
		// Amount to move in one frame.
		private static const kMoveSpeed:Number = 4.0;
		
		// Amount to move by before checking for collision.
		private static const kMoveIncrement:Number = 1.0;

		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		public var spritemap:Spritemap = new Spritemap(kSpritemapFile, 32, 32);
		
		[Embed(source='../sfx/playerhit.mp3')]
		private static const kHitSfxFile:Class;
		private var hitSfx:Sfx = new Sfx(kHitSfxFile);

		[Embed(source='../sfx/gameover.mp3')]
		private static const kDieSfxFile:Class;
		private var dieSfx:Sfx = new Sfx(kDieSfxFile);

		// 0.0 = facing right.
		// 90.0 = facing up.
		public var angle:Number = 270.0;
		
		public var health:int = maxHealth;
		
		private var hitTimer:int = 0;
		
		private static const kHitDuration:int = 120;

		public function Player(x:Number, y:Number) {
			if (GameWorld.difficulty == 2) {
				// hard mode
				maxHealth = 5;
			} else {
				maxHealth = 7;
			}
			
			health = maxHealth;
			
			addAnimations();

			setHitbox(32, 32);
			spritemap.originX = halfWidth;
			spritemap.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;

			type = "player";
			name = "player";
			layer = -100;
			
			super(x, y, spritemap);
		}

		override public function update():void {
			if (health <= 0) {
				// We're dead.
				return;
			}
			
			// Movement.
			handleMovement();
			handleFacing();
			
			// Hit timer.
			if (hitTimer > 0) {
				hitTimer--;
			}

			if (hitTimer % 4 == 1 || hitTimer % 4 == 2) {
				graphic.visible = false;
			} else {
				graphic.visible = true;
			}
		}
		
		public function hit():void {
			if (hitTimer > 0 || GameWorld.world().levelCompleted) {
				// invincible.
				return;
			}
			
			hitSfx.play();
			health--;
			hitTimer = kHitDuration;
			
			if (spritemap.currentAnim.charAt(0) != 'h') {
				spritemap.play('h' + spritemap.currentAnim);
			}
			
			if (health <= 0) {
				visible = false;
				GameWorld.world().gameOver();
				dieSfx.play();
			}
		}
		
		private function addAnimations():void {
			spritemap.add("s", [0, 1], 0.1);
			spritemap.add("w", [2, 3], 0.1);
			spritemap.add("e", [4, 5], 0.1);
			spritemap.add("n", [6, 7], 0.1);
			spritemap.add("sw", [8, 9], 0.1);
			spritemap.add("se", [10, 11], 0.1);
			spritemap.add("nw", [12, 13], 0.1);
			spritemap.add("ne", [14, 15], 0.1);
			spritemap.add("hs", [16+0, 16+1], 0.1);
			spritemap.add("hw", [16+2, 16+3], 0.1);
			spritemap.add("he", [16+4, 16+5], 0.1);
			spritemap.add("hn", [16+6, 16+7], 0.1);
			spritemap.add("hsw", [16+8, 16+9], 0.1);
			spritemap.add("hse", [16+10, 16+11], 0.1);
			spritemap.add("hnw", [16+12, 16+13], 0.1);
			spritemap.add("hne", [16 + 14, 16 + 15], 0.1);
		}
		
		private function handleFacing():void {
			// Can't turn while shooting.
			if (Input.check("fire")) {
				return;
			}
			
			var hit:String = "";
			if (hitTimer > 0) {
				hit = "h";
			}
			
			if (Input.check(Key.LEFT) && !Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				angle = 180.0;
				spritemap.play(hit+"w");
			} else if (!Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				angle = 90.0;
				spritemap.play(hit+"n");
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				angle = 270.0;
				spritemap.play(hit+"s");
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && !Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				angle = 0.0;
				spritemap.play(hit+"e");
			} else if (Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				angle = 135.0;
				spritemap.play(hit+"nw");
			} else if (Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				angle = 225.0;
				spritemap.play(hit+"sw");
			} else if (!Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				angle = 45.0;
				spritemap.play(hit+"ne");
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				angle = 315.0;
				spritemap.play(hit+"se");
			}
		}
		
		private function handleMovement():void {
			var xVel:Number = 0.0;
			var yVel:Number = 0.0;
			
			if (Input.check(Key.LEFT) && !Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				xVel -= kMoveIncrement;
			} else if (!Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				yVel -= kMoveIncrement;
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				yVel += kMoveIncrement;
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && !Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				xVel += kMoveIncrement;
			} else if (Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				xVel -= kMoveIncrement / Math.sqrt(2.0);
				yVel -= kMoveIncrement / Math.sqrt(2.0);
			} else if (Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && !Input.check(Key.RIGHT)) {
				xVel -= kMoveIncrement / Math.sqrt(2.0);
				yVel += kMoveIncrement / Math.sqrt(2.0);
			} else if (!Input.check(Key.LEFT) && Input.check(Key.UP) && !Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				xVel += kMoveIncrement / Math.sqrt(2.0);
				yVel -= kMoveIncrement / Math.sqrt(2.0);
			} else if (!Input.check(Key.LEFT) && !Input.check(Key.UP) && Input.check(Key.DOWN) && Input.check(Key.RIGHT)) {
				xVel += kMoveIncrement / Math.sqrt(2.0);
				yVel += kMoveIncrement / Math.sqrt(2.0);
			}
			
			// Try moving in x.
			for (var xMoveDistance:Number = 0.0; xMoveDistance < kMoveSpeed; xMoveDistance += kMoveIncrement) {
				var prevX:Number = x;
				x += xVel;
				if (collide("level", x, y)) {
					x = prevX;
					break;
				}
			}
				
			// Try moving in y.
			for (var yMoveDistance:Number = 0.0; yMoveDistance < kMoveSpeed; yMoveDistance += kMoveIncrement) {
				var prevY:Number = y;
				y += yVel;
				if (collide("level", x, y)) {
					y = prevY;
					break;
				}
			}
			
			// Update camera.
			FP.setCamera(x - FP.halfWidth, y - FP.halfHeight - 25);
		}
	}
}
