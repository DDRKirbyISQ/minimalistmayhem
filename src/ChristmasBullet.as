package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ChristmasBullet extends Bullet {
		[Embed(source='../img/christmasbullet1.png')]
		private static const kImageFile1:Class;
		[Embed(source='../img/christmasbullet2.png')]
		private static const kImageFile2:Class;
		[Embed(source='../img/christmasbullet3.png')]
		private static const kImageFile3:Class;
		[Embed(source='../img/christmasbullet4.png')]
		private static const kImageFile4:Class;
		[Embed(source='../img/christmasbullet5.png')]
		private static const kImageFile5:Class;
		[Embed(source='../img/christmasbullet6.png')]
		private static const kImageFile6:Class;
		
		[Embed(source='../img/particlemedium.png')]
		private static const kParticleFile:Class;
		
		private static const kImageFiles:Vector.<Class> = Vector.<Class>([kImageFile1, kImageFile2, kImageFile3, kImageFile4, kImageFile5, kImageFile6]);
		
		private var image:Image = new Image(kImageFiles[FP.rand(6)]);
		
		private static var speed:Number;
		
		public function ChristmasBullet() {
			switch (GameWorld.difficulty) {
				case 0:
					speed = 2.0;
					break;
				case 1:
					speed = 3.0;
					break;
				case 2:
					speed = 4.0;
					break;
			}
			
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, angle:Number):void
		{
			super.init(x, y, angle, speed, 0);
			image.angle = angle;
			image.smooth = true;
		}

		override protected function onPlayerCollide(player:Player):void {
			player.hit();
			explode();
			FP.world.recycle(this);
		}

		override protected function explode():void {
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0xFF8080);
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0x80FF80);
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0x8080FF);
		}
	}
}
