package {
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintbrushWeapon extends Weapon {
		[Embed(source='../sfx/paintbrush.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function PaintbrushWeapon() {
			super(30, -1);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			sfx.play();
			var bullet:PaintbrushBullet = GameWorld.world().create(PaintbrushBullet) as PaintbrushBullet;
			bullet.reset(x, y, angle);
		}
	}
}
