package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class NoteBullet extends Bullet {
		[Embed(source='../img/notebullet.png')]
		private static const kImageFile:Class;
		
		[Embed(source='../img/particlemedium.png')]
		private static const kParticleFile:Class;
		
		private var image:Image = new Image(kImageFile);
		
		private static var speed:Number;
		
		private var angleTimer:Number = FP.random * 360;
		
		public function NoteBullet() {
			switch (GameWorld.difficulty) {
				case 0:
					speed = 1.5;
					break;
				case 1:
					speed = 2.0;
					break;
				case 2:
					speed = 3.0;
					break;
			}

			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, angle:Number):void
		{
			super.init(x, y, angle, speed, 0);
		}

		override protected function onPlayerCollide(player:Player):void {
			player.hit();
			explode();
			FP.world.recycle(this);
		}
		
		override public function update():void {
			super.update();
			
			angleTimer++;
			angle += Math.sin(angleTimer * FP.RAD);
		}

		override protected function explode():void {
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0xFF8080);
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0x80FF80);
			ParticleExplosion.make(x, y, kParticleFile, 3, 3, 4, 48, 48, 8, 8, 0x8080FF);
		}
	}
}
