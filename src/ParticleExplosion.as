package {
	import flash.system.IMEConversionMode;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ParticleExplosion extends Entity {
		private var emitter:Emitter;
		
		public static function make(x:Number, y:Number, particleFile:Class, width:int, height:int, number:int, distance:int, distanceRange:int, duration:int, durationRange:int, color:uint):void {
			var particleExplosion:ParticleExplosion = FP.world.create(ParticleExplosion) as ParticleExplosion;
			particleExplosion.reset(x, y, particleFile, width, height, number, distance, distanceRange, duration, durationRange, color);
		}
		
		public function reset(x:Number, y:Number, particleFile:Class, width:int, height:int, number:int, distance:int, distanceRange:int, duration:int, durationRange:int, color:uint):void {
			this.x = x;
			this.y = y;
			
			layer = -25;
			
			emitter = new Emitter(particleFile, width, height);
			emitter.newType("regular", [0]);
			emitter.setAlpha("regular", 1, 0);
			emitter.setMotion("regular", 0, distance, duration, 360, distanceRange, durationRange);
			emitter.setColor("regular", color, color);
			graphic = emitter;
			for (var i:int = 0; i < number; ++i) {
				emitter.emit("regular", 0, 0);
			}
		}
		
		override public function update():void {
			if (emitter.particleCount <= 0) {
				FP.world.recycle(this);
			}
		}
	}

}