package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ColorWheelBullet extends Bullet {
		[Embed(source='../img/colorwheelbullet.png')]
		private static const kImageFile:Class;
		
		[Embed(source='../img/particlelarge.png')]
		private static const kParticleFile:Class;
		
		private var image:Image = new Image(kImageFile);
		private var spinDirection:int = FP.rand(2) * 2 - 1;
		
		private static var speed:Number;
		
		public function ColorWheelBullet() {
			switch (GameWorld.difficulty) {
				case 0: 
					speed = 2.0;
					break;
				case 1: 
					speed = 3.0;
					break;
				case 2: 
					speed = 4.0;
					break;
			}
			
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		override public function update():void {
			super.update();
			image.angle += 10.0 * spinDirection;
		}
		
		public function reset(x:Number, y:Number, angle:Number):void {
			super.init(x, y, angle, speed, 0);
			image.smooth = true;
		}
		
		override protected function onPlayerCollide(player:Player):void {
			player.hit();
			explode();
			FP.world.recycle(this);
		}
		
		override protected function explode():void {
			ParticleExplosion.make(x, y, kParticleFile, 8, 8, 4, 48, 48, 8, 8, 0xFF8080);
			ParticleExplosion.make(x, y, kParticleFile, 8, 8, 4, 48, 48, 8, 8, 0x80FF80);
			ParticleExplosion.make(x, y, kParticleFile, 8, 8, 4, 48, 48, 8, 8, 0x8080FF);
		}
	}
}
