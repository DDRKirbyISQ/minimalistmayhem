package {
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintzookaWeapon extends Weapon {
		[Embed(source='../sfx/paintzooka.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function PaintzookaWeapon() {
			super(45, 30);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			sfx.play();
			var bullet:PaintzookaBullet = GameWorld.world().create(PaintzookaBullet) as PaintzookaBullet;
			bullet.reset(x, y, angle);
		}
	}
}
