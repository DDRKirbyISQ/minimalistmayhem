package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ScreenText extends Entity {
		private const kFadeRate:Number = 0.05;
		
		private var text:Text;
		
		private var timer:int;
		
		public function reset(textString:String, duration:int, color:uint, alpha:Number):void {
			var oldText:ScreenText = FP.world.getInstance("screentext") as ScreenText;
			if (oldText) {
				FP.world.recycle(oldText);
			}

			text = new Text(textString, FP.halfWidth, FP.halfHeight + 100);
			text.alpha = alpha;
			text.scrollX = 0;
			text.scrollY = 0;
			text.scale = 3;
			text.centerOrigin();
			graphic = text;
			name = "screentext";
			
			layer = -1000;
			timer = duration;
		}
		
		override public function update():void {
			if (timer > 0) {
				timer--;
				return;
			}
			
			text.alpha -= kFadeRate;
			if (text.alpha <= 0) {
				world.recycle(this);
			}
		}
	}
}
