package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	// Straight line traveling bullet.
	public class Bullet extends Entity {
		[Embed(source='../sfx/wallhit.mp3')]
		private static const kWallSfxFile:Class;
		private var wallSfx:Sfx = new Sfx(kWallSfxFile);
		
		[Embed(source='../img/particlesmall.png')]
		private static const kParticleFile:Class;
		
		public var angle:Number;
		public var speed:Number;
		public var paintColor:int;
		
		public function Bullet() {
			type = "bullet";
			layer = -70;
		}
		
		public function init(x:Number, y:Number, angle:Number, speed:Number, paintColor:int):void {
			this.x = x;
			this.y = y;
			this.angle = angle;
			this.speed = speed;
			
			// 0 = enemy
			// 1 = player
			// -1 = neutral
			this.paintColor = paintColor;
		}
		
		protected function onPlayerCollide(player:Player):void {
		}
		
		protected function onEnemyCollide(enemy:Enemy):void {
		}
		
		protected function paintLevel(tileX:int, tileY:int):void {
			if (paintColor == 0 || paintColor == 1) {
				var level:Level = FP.world.getInstance("level");
				
				// Only paint tiles that haven't been painted this frame.
				/*for each (var v:Vector.<int> in GameWorld.world().paintedTilesThisFrame) {
				   if (v[0] == tileX && v[1] == tileY) {
				   return;
				   }
				 }*/
				
				var originalTile:uint = level.tilemap.getTile(tileX, tileY);
				var newTile:uint = originalTile;
				if (newTile % 2 != 0) {
					newTile--;
				}
				
				newTile += paintColor;
				
				if (newTile != originalTile) {
					level.tilemap.setTile(tileX, tileY, newTile);
					
						// Particle effect.
					/*
					   var centerX:Number = (tileX + 0.5) * level.tilemap.tileWidth;
					   var centerY:Number = (tileY + 0.5) * level.tilemap.tileHeight;
					   if (paintColor == 1) {
					   ParticleExplosion.make(centerX, centerY, kParticleFile, 1, 1, 5, 32, 32, 30, 30, 0xFFFFFF);
					   } else if (paintColor == 0) {
					   ParticleExplosion.make(centerX, centerY, kParticleFile, 1, 1, 5, 32, 32, 30, 30, 0xFF8080);
					   ParticleExplosion.make(centerX, centerY, kParticleFile, 1, 1, 5, 32, 32, 30, 30, 0x80FF80);
					   ParticleExplosion.make(centerX, centerY, kParticleFile, 1, 1, 5, 32, 32, 30, 30, 0x8080FF);
					   }
					
					   var newV:Vector.<int> = Vector.<int>([tileX, tileY]);
					 GameWorld.world().paintedTilesThisFrame.push(newV);*/
				}
			}
		}
		
		override public function update():void {
			x += Math.cos(angle * FP.RAD) * speed;
			y += Math.sin(angle * FP.RAD) * speed;
			
			var level:Level = FP.world.getInstance("level");
			paintLevel((x + halfWidth) / level.grid.tileWidth, (y + halfHeight) / level.grid.tileHeight);
			paintLevel((x + halfWidth) / level.grid.tileWidth, (y - halfHeight) / level.grid.tileHeight);
			paintLevel((x - halfWidth) / level.grid.tileWidth, (y + halfHeight) / level.grid.tileHeight);
			paintLevel((x - halfWidth) / level.grid.tileWidth, (y - halfHeight) / level.grid.tileHeight);
			
			// Sanity check - delete if you go off the world bounds.
			if (x < 0 || x > level.width || y < 0 || y > level.height) {
				FP.world.recycle(this);
			}
			
			var player:Player = FP.world.getInstance("player");
			// Delete if too far off screen
			if (Math.abs(x - player.x) > FP.width * 0.66 || Math.abs(y - player.y) > FP.width * 0.66) {
				FP.world.recycle(this);
			}
			
			player = collide("player", x, y) as Player;
			if (player) {
				onPlayerCollide(player);
			}
			
			if (multipleEnemyCollide()) {
				var enemyArray:Vector.<Enemy> = new Vector.<Enemy>;
				collideInto("enemy", x, y, enemyArray);
				for each (var curEnemy:Enemy in enemyArray) {
					onEnemyCollide(curEnemy);
				}
			} else {
				var enemy:Enemy = collide("enemy", x, y) as Enemy;
				if (enemy) {
					onEnemyCollide(enemy);
				}
			}
			level = collide("level", x, y) as Level;
			if (level) {
				if (levelCollision()) {
					explode();
					if (wallSound()) {
						wallSfx.play();
					}
					FP.world.recycle(this);
				}
			}
		}
		
		protected function wallSound():Boolean {
			return true;
		}
		
		protected function levelCollision():Boolean {
			return true;
		}
		
		protected function multipleEnemyCollide():Boolean {
			return false;
		}
		
		protected function explode():void {
		}
	}
}
