package {
	import net.flashpunk.Sfx;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class SpraypaintWeapon extends Weapon {
		[Embed(source='../sfx/spraypaint.mp3')]
		private static const kSfxFile:Class;
		private static var sfx:Sfx = new Sfx(kSfxFile);

		public function SpraypaintWeapon() {
			super(2, 500);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			if (!sfx.playing) {
				sfx.play();
			}
			for (var i:int = -3; i <= 3; ++i) {
				var angleMod:Number = i * 10;
				angleMod += 2 * (FP.random - 0.5) * 20;
				var xMod:Number = (FP.random - 0.5) * 10;
				var yMod:Number = (FP.random - 0.5) * 10;
				var bullet:SpraypaintBullet = GameWorld.world().create(SpraypaintBullet) as SpraypaintBullet;
				bullet.reset(x + xMod, y + yMod, angle + angleMod);
			}
		}
	}
}
