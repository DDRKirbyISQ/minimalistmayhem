package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ColorWheelEnemy extends Enemy {
		[Embed(source='../img/colorwheel.png')]
		private static const kSpriteFile:Class;
		private var image:Image = new Image(kSpriteFile);
		
		[Embed(source='../sfx/colorwheelbullet.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);
		
		private static const kMoveSpeed:Number = 0.5;
		private static var kCooldown:int = 120;
		private static const kHealth:Number = 5.0;
		
		private var spinDirection:int = FP.rand(2) * 2 - 1;
		
		public function ColorWheelEnemy() {
			if (GameWorld.difficulty == 2) {
				kCooldown = 90;
			} else {
				kCooldown = 120;
			}

			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			
			graphic = image;
		}
		
		override public function update():void {
			super.update();
			image.angle += 10 * spinDirection;
		}
		
		public function reset(x:Number, y:Number):void {
			super.init(x, y, kCooldown, kHealth);
		}
		
		override protected function onFire(playerAngle:Number):void {
			var bullet:ColorWheelBullet = FP.world.create(ColorWheelBullet) as ColorWheelBullet;
			bullet.reset(x, y, playerAngle);
			sfx.play();
		}
		
		override protected function onMove(playerAngle:Number):void {
			var player:Player = world.getInstance("player");
			if (distanceFrom(player) > 100) {
				tryMove(Math.cos(playerAngle) * kMoveSpeed, Math.sin(playerAngle) * kMoveSpeed);
			}
		}
	}
}
