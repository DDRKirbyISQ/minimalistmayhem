package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ChristmasTreeEnemy extends Enemy {
		[Embed(source='../img/christmastreeenemy.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 43, 53);
		
		[Embed(source='../sfx/christmasbullet.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);
		
		private static const kMoveSpeed:Number = 0.5;
		private static var kCooldown:int = 120;
		private static const kHealth:Number = 5.0;
		
		public function ChristmasTreeEnemy() {
			if (GameWorld.difficulty == 2) {
				kCooldown = 90;
			} else {
				kCooldown = 120;
			}

			setHitbox(spritemap.width, spritemap.height);
			spritemap.originX = halfWidth;
			spritemap.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			
			spritemap.add("s", [0, 1], 0.05);
			spritemap.play("s");
			graphic = spritemap;
		}
		
		public function reset(x:Number, y:Number):void {
			super.init(x, y, kCooldown, kHealth);
		}
		
		override protected function onFire(playerAngle:Number):void {
			for (var i:int = -1; i <= 1; ++i) {
				var angleMod:Number = i * 50.0;
				var bullet:ChristmasBullet = FP.world.create(ChristmasBullet) as ChristmasBullet;
				bullet.reset(x, y, playerAngle + angleMod);
				sfx.play();
			}
		}
		
		override protected function onMove(playerAngle:Number):void {
			var player:Player = world.getInstance("player");
			if (distanceFrom(player) > 100) {
				tryMove(Math.cos(playerAngle) * kMoveSpeed, Math.sin(playerAngle) * kMoveSpeed);
			}
		}
	}
}
