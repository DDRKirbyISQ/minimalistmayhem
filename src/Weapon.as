package {
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Weapon {
		protected var cooldownTimer:int = 0;
		protected var cooldown:int;
		public var ammo:int = 0;
		
		public function Weapon(cooldown:int, ammo:int) {
			// If -1, infinite ammo.
			if (GameWorld.difficulty == 0) {
				// easy mode
				ammo *= 2;
			}
			this.ammo = ammo;
			this.cooldown = cooldown;
		}
		
		// Does the actual firing of the weapon.
		protected function doShoot(x:Number, y:Number, angle:Number):void {
		}
		
		public function update():void {
			if (cooldownTimer > 0) {
				--cooldownTimer;
			}
			
			var player:Player = GameWorld.world().getInstance("player");
			
			if (Input.check("fire") && player.health > 0) {
				tryShoot();
			}
		}
		
		public function tryShoot():void {
			if (cooldownTimer <= 0) {
				var player:Player = GameWorld.world().getInstance("player");
				doShoot(player.x, player.y, player.angle);
				cooldownTimer = cooldown;
				if (ammo > 0) {
					ammo--;
					if (ammo == 0) {
						// Switch weapons to basic weapon.
						GameWorld.world().weapon = new PaintbrushWeapon();
					}
				}
			}
		}
	}
}
