package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MSPaintPowerup extends Powerup
	{
		[Embed(source='../img/mspaintpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		public function MSPaintPowerup()
		{
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
		}
		
		override protected function onPickup():void {
			GameWorld.world().weapon = new MSPaintWeapon;
			var screenText:ScreenText = world.create(ScreenText) as ScreenText;
			screenText.reset("MICROSOFT PAINT", 60, 0xFFFFFF, 1.0);
		}
	}
}
