package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.masks.Hitbox;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class SpraypaintBullet extends Bullet {
		[Embed(source='../img/particlelarge.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		private static const kDuration:int = 10;
		
		private var life:int;
		
		private static const kDamage:Number = 0.2;
		
		private static const kSpeed:Number = 20;
		
		public function reset(x:Number, y:Number, angle:Number):void {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
			
			super.init(x, y, angle, kSpeed, 1);
			life = kDuration;
		}
		
		override protected function onEnemyCollide(enemy:Enemy):void {
			enemy.hit(kDamage);
			explode();
			FP.world.recycle(this);
		}
		
		override public function update():void {
			super.update();
			--life;
			if (life <= 0) {
				explode();
				FP.world.recycle(this);
			}
		}
		
		override protected function wallSound():Boolean {
			return false;
		}
	}
}
