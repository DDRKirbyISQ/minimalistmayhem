package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.Screen;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Room extends Entity {
		[Embed(source='../sfx/roomcomplete.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		private static var kClearRatio:Number = 0.7;
		
		private var roomWidth:Number;
		private var roomHeight:Number;
		
		public var complete:Boolean;
		
		public var hasEntered:Boolean = false;
		
		private var text:Text = new Text("ROOM INCOMPLETE...", 0, 0);
		
		public function Room(x:Number, y:Number, width:Number, height:Number) {
			super(x, y);
			this.roomWidth = width;
			this.roomHeight = height;
			
			setHitbox(width, height);
			complete = false;
			text.visible = false;
			text.centerOrigin();
			text.x = width / 2;
			text.y = height / 2;
			text.scale = 3;
			addGraphic(text);
			layer = -500;
		}
		
		override public function update():void {
			if (GameWorld.difficulty == 0) {
				kClearRatio = 0.6;
			} else {
				kClearRatio = 0.7;
			}
			
			if (complete) {
				text.visible = false;
				return;
			}
			
			if (!hasEntered) {
				if (playerInRoom()) {
					hasEntered = true;
				}
			}
			
			text.visible = hasEntered && !playerInRoom();
			
			// Check for enemies.
			if (collide("enemy", x, y)) {
				return;
			}
			
			var playerTiles:int = 0;
			var enemyTiles:int = 0;
			
			var tilemap:Tilemap = GameWorld.world().level.tilemap;
			
			var minX:int = x / tilemap.tileWidth;
			var minY:int = y / tilemap.tileHeight;
			var maxX:int = (x + roomWidth) / tilemap.tileWidth;
			var maxY:int = (y + roomHeight) / tilemap.tileHeight;
			
			for (var curX:int = minX; curX <= maxX; ++curX) {
				for (var curY:int = minY; curY <= maxY; ++curY) {
					var tileIndex:uint = GameWorld.world().level.tilemap.getTile(curX, curY);
					if (tileIndex == 10 || tileIndex == 11) {
						// Blank tile.
						continue;
					}
					
					if (tileIndex % 2 == 0) {
						enemyTiles++;
					} else {
						playerTiles++;
					}
				}
			}
			
			var ratio:Number = Number(playerTiles) / (playerTiles + enemyTiles);
			if (ratio > kClearRatio) {
				completeRoom();
			}
		}
		
		private function completeRoom():void {
			complete = true;
			sfx.play();
			
			var tilemap:Tilemap = GameWorld.world().level.tilemap;
			
			var minX:int = x / tilemap.tileWidth;
			var minY:int = y / tilemap.tileHeight;
			var maxX:int = (x + roomWidth) / tilemap.tileWidth;
			var maxY:int = (y + roomHeight) / tilemap.tileHeight;
			
			for (var curX:int = minX - 1; curX <= maxX + 1; ++curX) {
				for (var curY:int = minY - 1; curY <= maxY + 1; ++curY) {
					var originalTile:uint = tilemap.getTile(curX, curY);
					var newTile:uint = originalTile;
					if (newTile % 2 != 1) {
						newTile++;
					}
					tilemap.setTile(curX, curY, newTile);
				}
			}
			
			var screenFlash:ScreenFlash = world.create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 1.0);
			var screenText:ScreenText = world.create(ScreenText) as ScreenText;
			if (GameWorld.world().completedRooms() == GameWorld.world().totalRooms()) {
				if (GameWorld.levelNumber == 3) {
					// WIN
					screenText.reset("CONGRATULATIONS\n    YOU WIN!", 1200, 0xFFFFFF, 1.0);
				} else {
					screenText.reset("LEVEL COMPLETE", 1200, 0xFFFFFF, 1.0);
				}
				GameWorld.world().completeLevel();
			} else {
				screenText.reset("ROOM COMPLETE", 120, 0xFFFFFF, 1.0);
			}
		}
		
		private function playerInRoom():Boolean {
			var player:Player = GameWorld.world().getInstance("player") as Player;
			if (player.x > x - 16 && player.x < x + roomWidth + 16 && player.y > y - 16 && player.y < y + roomHeight + 16) {
				return true;
			}
			return false;
		}
	}
}
