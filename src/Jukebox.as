package {
	import flash.sampler.NewObjectSample;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Jukebox extends Enemy {
		[Embed(source='../img/jukebox.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/jukeboxdie.mp3')]
		private static const kDieSfxFile:Class;
		private var dieSfx:Sfx = new Sfx(kDieSfxFile);

		[Embed(source='../sfx/jukeboxnote.mp3')]
		private static const kNoteSfxFile:Class;
		private var noteSfx:Sfx = new Sfx(kNoteSfxFile);

		[Embed(source='../mus/MinimalistMayhem_bass.mp3')]
		private static const kLayer1File:Class;
		public static var layer1Sfx:Sfx = new Sfx(kLayer1File);
		[Embed(source='../mus/MinimalistMayhem_midbass.mp3')]
		private static const kLayer2File:Class;
		public static var layer2Sfx:Sfx = new Sfx(kLayer2File);
		[Embed(source='../mus/MinimalistMayhem_drums.mp3')]
		private static const kLayer3File:Class;
		public static var layer3Sfx:Sfx = new Sfx(kLayer3File);
		[Embed(source='../mus/MinimalistMayhem_lead.mp3')]
		private static const kLayer4File:Class;
		public static var layer4Sfx:Sfx = new Sfx(kLayer4File);
		[Embed(source='../mus/MinimalistMayhem_arp.mp3')]
		private static const kLayer5File:Class;
		public static var layer5Sfx:Sfx = new Sfx(kLayer5File);
		
		public static var sfxArray:Vector.<Sfx> = Vector.<Sfx>([layer5Sfx, layer2Sfx, layer3Sfx, layer1Sfx, layer4Sfx]);

		private static var cooldown:int;
		
		private static const kHealth:Number = 150;
		
		private var musicLayer:int;
		
		public function Jukebox(x:Number, y:Number, musicLayer:int) {
			switch (GameWorld.difficulty) {
				case 0:
					cooldown = 30;
					break;
				case 1:
					cooldown = 20;
					break;
				case 2:
					cooldown = 15;
					break;
			}

			super();
			init(x, y, cooldown, kHealth);
			graphic = image;
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			this.musicLayer = musicLayer;
			
			layer = -25;
		}
		
		override protected function onFire(playerAngle:Number):void {
			var angleMod:Number = (FP.random - 0.5) * 360;
			var bullet:NoteBullet = FP.world.create(NoteBullet) as NoteBullet;
			bullet.reset(x - 40, y + 10, playerAngle + angleMod);
			var angleMod2:Number = (FP.random - 0.5) * 360;
			var bullet2:NoteBullet = FP.world.create(NoteBullet) as NoteBullet;
			bullet2.reset(x + 40, y + 10, playerAngle + angleMod2);
			noteSfx.play();
		}
		
		override public function update():void {
			if (GameWorld.world().gameTimer == 1) {
				sfxArray[musicLayer].loop();
			}
			
			super.update();
		}

		override protected function onDie():void {
			dieSfx.play();
			
			var screenFlash:ScreenFlash = world.create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 0.5);
			
			sfxArray[musicLayer].volume = 0;

			var screenText:ScreenText = world.create(ScreenText) as ScreenText;
			screenText.reset("JUKEBOX DESTROYED", 120, 0xFFFFFF, 1.0);
			
			var heart:Heart = world.create(Heart) as Heart;
			heart.reset(x, y);
		}
	}
}
