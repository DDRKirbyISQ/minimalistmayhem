package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Map extends Entity {
		private static const kSize:Number = 150;
		private static const kPadding:Number = 30;
		
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 32, 32);
		
		public function Map() {
			spritemap.scale = 0.33;
			spritemap.add("s", [0, 1], 0.1);
			spritemap.add("w", [2, 3], 0.1);
			spritemap.add("e", [4, 5], 0.1);
			spritemap.add("n", [6, 7], 0.1);
			spritemap.add("sw", [8, 9], 0.1);
			spritemap.add("se", [10, 11], 0.1);
			spritemap.add("nw", [12, 13], 0.1);
			spritemap.add("ne", [14, 15], 0.1);
			spritemap.add("hs", [16 + 0, 16 + 1], 0.1);
			spritemap.add("hw", [16 + 2, 16 + 3], 0.1);
			spritemap.add("he", [16 + 4, 16 + 5], 0.1);
			spritemap.add("hn", [16 + 6, 16 + 7], 0.1);
			spritemap.add("hsw", [16 + 8, 16 + 9], 0.1);
			spritemap.add("hse", [16 + 10, 16 + 11], 0.1);
			spritemap.add("hnw", [16 + 12, 16 + 13], 0.1);
			spritemap.add("hne", [16 + 14, 16 + 15], 0.1);
			//spritemap.originX = 8;
			//spritemap.originY = 8;
			centerOrigin();
			spritemap.centerOrigin();
			
			layer = -1000;
		}
		
		override public function update():void {
			var player:Player = world.getInstance("player");
			spritemap.play(player.spritemap.currentAnim);
		}
		
		override public function render():void {
			Draw.setTarget(FP.buffer, null);
			
			var x:int = FP.width - kSize - kPadding;
			var y:int = FP.height - kSize - kPadding;
			
			// Background rectangle.
			Draw.rect(x, y, kSize, kSize, 0x606060, 0.5);
			
			var level:Level = GameWorld.world().level;
			
			// Grid?
			/*			for (var col:int = 0; col < Level.TILE_MAP_WIDTH / 32; ++col) {
			   for (var row:int = 0; row < Level.TILE_MAP_HEIGHT / 32; ++row) {
			   if (level.tilemap.getTile(col, row) < 8) {
			   var tileX:Number = col * 32.0 / Level.TILE_MAP_WIDTH * kSize;
			   var tileY:Number = row * 32.0 / Level.TILE_MAP_HEIGHT * kSize;
			   var tileWidth:Number = 32.0 / Level.TILE_MAP_WIDTH  * kSize;
			   var tileHeight:Number = 32.0 / Level.TILE_MAP_HEIGHT * kSize;
			   Draw.rect(x + tileX, y + tileY, tileWidth, tileHeight, 0xFFFFFF, 0.5);
			   }
			   }
			 }*/
			
			// Rooms.
			var rooms:Vector.<Room> = new Vector.<Room>;
			world.getClass(Room, rooms);
			for each (var room:Room in rooms) {
				var color:uint = 0xFF0000;
				if (room.hasEntered && !room.complete) {
					if (GameWorld.world().gameTimer % 60 < 30) {
						color = 0xFF0000;
					} else {
						color = 0xFFFF00;
					}
				}
				if (room.complete) {
					color = 0x80FF80;
				}
				var roomX:Number = room.x / Level.TILE_MAP_WIDTH * kSize;
				var roomY:Number = room.y / Level.TILE_MAP_HEIGHT * kSize;
				var roomWidth:Number = room.width / Level.TILE_MAP_WIDTH * kSize;
				var roomHeight:Number = room.height / Level.TILE_MAP_HEIGHT * kSize;
				Draw.rect(x + roomX, y + roomY, roomWidth, roomHeight, color, 0.85);
			}
			
			if (GameWorld.levelNumber == 4) {
				// gauntlet.
				var factories:Vector.<EnemyFactory> = new Vector.<EnemyFactory>;
				world.getClass(EnemyFactory, factories);
				for each (var enemyFactory:EnemyFactory in factories) {
					var fX:Number = enemyFactory.x / Level.TILE_MAP_WIDTH * kSize;
					var fY:Number = enemyFactory.y / Level.TILE_MAP_HEIGHT * kSize;
					var fWidth:Number = enemyFactory.width / Level.TILE_MAP_WIDTH * kSize;
					var fHeight:Number = enemyFactory.height / Level.TILE_MAP_HEIGHT * kSize;
					Draw.rect(x + fX, y + fY, fWidth, fHeight, 0xFF0000, 0.85);
				}
				
			}
			
			// Player.
			var player:Player = world.getInstance("player");
			var playerX:Number = player.x / Level.TILE_MAP_WIDTH * kSize;
			var playerY:Number = player.y / Level.TILE_MAP_HEIGHT * kSize;
			Draw.graphic(spritemap, x + playerX, y + playerY);
		}
	}
}
