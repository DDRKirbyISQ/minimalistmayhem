package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	// Marks where the player should spawn.
	public class SpawnPoint extends Entity {
		public function SpawnPoint(x:Number, y:Number) {
			super(x, y);
			name = "spawnpoint";
		}
	}
}
