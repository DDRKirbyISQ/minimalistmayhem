package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.graphics.TiledImage;
	import net.flashpunk.FP;
	import flash.utils.ByteArray;
	import net.flashpunk.Mask;
	import net.flashpunk.masks.Grid;
	import net.pixelpracht.tmx.TmxLayer;
	import flash.geom.Point;
	import net.pixelpracht.tmx.TmxObject;
	import net.pixelpracht.tmx.TmxObjectGroup;
	import net.pixelpracht.tmx.TmxTileSet;
	import net.flashpunk.masks.Pixelmask;
	import flash.display.BitmapData;
	
	import net.pixelpracht.tmx.TmxMap;
	
	public class Level extends Entity {
		public static const TILE_MAP_WIDTH:int = 6400;
		public static const TILE_MAP_HEIGHT:int = 6400;
		private static const TILE_WIDTH:int = 32;
		private static const TILE_HEIGHT:int = 32;
		
		[Embed(source='../img/tilemap.png')]
		private var tileClass1:Class;
		
		//[Embed(source='../lvl/testlevel.tmx',mimeType="application/octet-stream")]
		//private static const kTestLevel:Class;
		
		[Embed(source='../lvl/level1.tmx',mimeType="application/octet-stream")]
		private static const kLevel1:Class;
		
		[Embed(source='../lvl/level2.tmx',mimeType="application/octet-stream")]
		private static const kLevel2:Class;

		[Embed(source='../lvl/level3.tmx',mimeType="application/octet-stream")]
		private static const kLevel3:Class;
		
		[Embed(source='../lvl/gauntlet.tmx',mimeType="application/octet-stream")]
		private static const kLevelGauntlet:Class;
		
		public var grid:Grid;
		public var tilemap:Tilemap;
		
		// Loads level from the given map file.
		public function Level(levelNumber:int) {
			var levelFile:Class;
			switch (levelNumber) {
				case 1: 
					levelFile = kLevel1;
					break;
				case 2: 
					levelFile = kLevel2;
					break;
				case 3: 
					levelFile = kLevel3;
					break;
				case 4: 
					levelFile = kLevelGauntlet;
					break;
			}
			
			tilemap = new Tilemap(tileClass1, TILE_MAP_WIDTH, TILE_MAP_HEIGHT, TILE_WIDTH, TILE_HEIGHT);
			var file:ByteArray = new levelFile;
			var csv:String = file.readUTFBytes(file.length);
			
			var xml:XML = FP.getXML(levelFile);
			var tmx:TmxMap = new TmxMap(xml);
			var tmxLayer:TmxLayer = tmx.getLayer('bg');
			var tileset:TmxTileSet = tmx.getTileSet('tilemap');
			var mapCsv:String = tmxLayer.toCsv(tileset);
			
			tilemap.loadFromString(mapCsv);
			graphic = tilemap;
			
			var collidableTiles:Array = new Array;
			
			for (var i:int = 8; i < 100; ++i) {
				collidableTiles.push(i);
			}
			
			grid = tilemap.createGrid(collidableTiles, grid);
			
			mask = grid;
			type = "level";
			name = "level";
			layer = 0;
			
			var tmxObjectGroup:TmxObjectGroup = tmx.getObjectGroup("objects");
			
			loadEntities(tmxObjectGroup);
			
			GameWorld.world().updateLists();
		}
		
		private function loadEntities(objectGroup:TmxObjectGroup):void {
			for each (var object:TmxObject in objectGroup.objects) {
				switch (object.type) {
					case "SpawnPoint": 
						GameWorld.world().add(new SpawnPoint(object.x + object.width / 2, object.y + object.height / 2));
						break;
					case "ChristmasTreeEnemy": 
						var christmasTreeEnemy:ChristmasTreeEnemy = GameWorld.world().create(ChristmasTreeEnemy) as ChristmasTreeEnemy;
						christmasTreeEnemy.reset(object.x + object.width / 2, object.y + object.height / 2);
						break;
					case "ColorWheelEnemy": 
						var colorWheelEnemy:ColorWheelEnemy = GameWorld.world().create(ColorWheelEnemy) as ColorWheelEnemy;
						colorWheelEnemy.reset(object.x + object.width / 2, object.y + object.height / 2);
						break;
					case "EnemyFactory": 
						var enemyFactory:EnemyFactory = GameWorld.world().create(EnemyFactory) as EnemyFactory;
						enemyFactory.reset(object.x + object.width / 2, object.y + object.height / 2);
						break;
					case "Room": 
						GameWorld.world().add(new Room(object.x, object.y, object.width, object.height));
						break;
					case "Jukebox": 
						GameWorld.world().add(new Jukebox(object.x + object.width / 2, object.y + object.height / 2, int(object.name)));
						break;
					case "Powerup": 
						switch (FP.rand(5)) {
						case 0: 
							var spraypaint:SpraypaintPowerup = GameWorld.world().create(SpraypaintPowerup) as SpraypaintPowerup;
							spraypaint.reset(object.x + object.width / 2, object.y + object.height / 2);
							break;
						case 1: 
							var paintball:PaintballPowerup = GameWorld.world().create(PaintballPowerup) as PaintballPowerup;
							paintball.reset(object.x + object.width / 2, object.y + object.height / 2);
							break;
						case 2: 
							var paintzooka:PaintzookaPowerup = GameWorld.world().create(PaintzookaPowerup) as PaintzookaPowerup;
							paintzooka.reset(object.x + object.width / 2, object.y + object.height / 2);
							break;
						case 3: 
							var mspaint:MSPaintPowerup = GameWorld.world().create(MSPaintPowerup) as MSPaintPowerup;
							mspaint.reset(object.x + object.width / 2, object.y + object.height / 2);
							break;
						case 4: 
							var paintspread:PaintSpreadPowerup = GameWorld.world().create(PaintSpreadPowerup) as PaintSpreadPowerup;
							paintspread.reset(object.x + object.width / 2, object.y + object.height / 2);
							break;
					}
				}
			}
		}
	}
}
