package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.masks.Hitbox;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintbrushBullet extends Bullet {
		[Embed(source='../img/paintbrush.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		private static const kDuration:int = 15;
		
		private var life:int;
		
		private static const kDamage:Number = 0.5;
		
		private static const kSpinRate:Number = -25;
		
		private var isPaintzookaExplosion:Boolean = false;
		
		public function PaintbrushBullet() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, angle:Number, isPaintzookaExplosion:Boolean = false):void {
			super.init(x, y, angle, 0, 1);
			life = kDuration;
			this.isPaintzookaExplosion = isPaintzookaExplosion;
		}
		
		override protected function onEnemyCollide(enemy:Enemy):void {
			enemy.hit(kDamage);
		}
		
		override public function update():void {
			if (!isPaintzookaExplosion) {
				var player:Player = world.getInstance("player");
				x = player.x;
				y = player.y;
				/*var bullet:Bullet = collide("bullet", x, y) as Bullet;
				if (bullet) {
					FP.world.recycle(bullet);
				}*/
			}

			image.angle += kSpinRate;

			super.update();
			
			--life;
			if (life <= 0) {
				explode();
				FP.world.recycle(this);
			}
			
			var level:Level = FP.world.getInstance("level");
			var paintX:Number = x;
			var paintY:Number = y;
			for (var xMod:Number = -60; xMod <= 60; xMod += 16) {
				for (var yMod:Number = -60; yMod <= 60; yMod += 16) {
					paintLevel((paintX + xMod) / level.grid.tileWidth, (paintY + yMod) / level.grid.tileHeight);
				}
			}
		}
		
		override protected function wallSound():Boolean {
			return false;
		}

		override protected function levelCollision():Boolean {
			return false;
		}

		override protected function multipleEnemyCollide():Boolean {
			return true;
		}
	}
}
