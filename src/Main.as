package {
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	[Frame(factoryClass="Preloader")]
	
	public class Main extends Engine {
		public function Main() {
			super(800, 600, 60, true);
			
			// Settings.
			Input.define("fire", Key.Z, Key.X, Key.SPACE);
			
			// Debug console.	
			//FP.console.enable();

			//sanity
			GameWorld.times[0] = 0;
			GameWorld.times[1] = 0;
			GameWorld.times[2] = 0;
			GameWorld.times[3] = 0;
			
			// Start game.
			FP.world = new MenuWorld;
		}
	}
}
