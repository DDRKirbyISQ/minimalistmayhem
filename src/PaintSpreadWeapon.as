package {
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class PaintSpreadWeapon extends Weapon {
		[Embed(source='../sfx/paintspread.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);
		
		public function PaintSpreadWeapon() {
			super(10, 100);
		}
		
		// Does the actual firing of the weapon.
		override protected function doShoot(x:Number, y:Number, angle:Number):void {
			for (var i:int = -1; i <= 1; ++i) {
				var angleMod:Number = i * 30.0;
				sfx.play();
				var bullet:PaintSpreadBullet = GameWorld.world().create(PaintSpreadBullet) as PaintSpreadBullet;
				bullet.reset(x, y, angle + angleMod);
			}
		}
	}
}
